#!/usr/bin/env node

/*

aethred-cli.js - Post a message for aethred.
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { login } from 'masto';
import fs from 'fs';

const HOST = process.env.MASTO_HOST;
const TOKEN = process.env.MASTO_TOKEN;
const NAME = process.env.MASTO_NAME;

async function main() {
  const message = fs.readFileSync(0, 'utf-8');

  const masto = await login( {
    url: `https://${HOST}`,
    accessToken: TOKEN,
  });

  masto.statuses.create({
    status: message,
    visibility: 'public',
  });
};

main();

